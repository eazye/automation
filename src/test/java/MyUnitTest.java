import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MyUnitTest {

    @Test
    public void testConcatenate() {
        MyUnit myUnit = new MyUnit();

        String result = myUnit.concatenate("one", "two");

        assertEquals("onetwo", result);

    }

    @Test
    public void testStringLength() {
        MyUnit myUnit = new MyUnit();
        String result = myUnit.concatenate("one", "two");
        result.length();
        assertEquals(6,6);
    }
}